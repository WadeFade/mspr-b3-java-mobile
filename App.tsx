import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginScreenCustom from './LoginScreen';
import WebViewCustom from './webView';

const Stack = createNativeStackNavigator();

const App = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    name="Home"
                    component={LoginScreenCustom}
                />
                <Stack.Screen name="Agents" component={WebViewCustom} />
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default App;
