import React from 'react';
import {WebView} from 'react-native-webview';

export default function WebViewCustom(props: any) {
    return (
        <WebView
            source={{uri: 'https://mobile.mspr.me/'}}
            javaScriptCanOpenWindowsAutomatically={true}
            basicAuthCredential={{'password': props.route.params.password, 'username': props.route.params.username}}
        />
    );
};
