import React, {useState} from 'react';
import LoginScreen from 'react-native-login-screen';

export default function LoginScreenCustom(props: any) {
    var usernametest = "";
    var passwordtest = "";

    return (
        <LoginScreen
            logoImageSource={require("./assets/icon.png")}
            onLoginPress={() => {
                props.navigation.navigate('Agents', {password: passwordtest, username: usernametest})
            }}
            onHaveAccountPress={() => {
            }}
            onEmailChange={(email: string) => {
                usernametest = email;
            }}
            onPasswordChange={(password: string) => {
                passwordtest = password;
            }}
            disableSocialButtons={true}
            disableDivider={true}
            haveAccountText={""}
        />
    );
}
